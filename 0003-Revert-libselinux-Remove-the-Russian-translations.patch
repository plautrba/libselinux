From e51435e095fcd2dbde3c7a0a4398012d037b9aa7 Mon Sep 17 00:00:00 2001
From: Petr Lautrbach <lautrbach@redhat.com>
Date: Mon, 13 Nov 2023 13:38:00 +0100
Subject: [PATCH] Revert "libselinux: Remove the Russian translations"
Content-type: text/plain

This reverts commit 84c195e18ccd9735c4eb947b1268d6d2b982cb5a.
---
 libselinux/man/ru/man8/avcstat.8            |  35 +++++++
 libselinux/man/ru/man8/booleans.8           |  46 +++++++++
 libselinux/man/ru/man8/getenforce.8         |  19 ++++
 libselinux/man/ru/man8/getsebool.8          |  40 ++++++++
 libselinux/man/ru/man8/matchpathcon.8       |  62 ++++++++++++
 libselinux/man/ru/man8/sefcontext_compile.8 |  70 +++++++++++++
 libselinux/man/ru/man8/selinux.8            | 106 ++++++++++++++++++++
 libselinux/man/ru/man8/selinuxenabled.8     |  21 ++++
 libselinux/man/ru/man8/selinuxexeccon.8     |  28 ++++++
 libselinux/man/ru/man8/setenforce.8         |  32 ++++++
 libselinux/man/ru/man8/togglesebool.8       |  23 +++++
 11 files changed, 482 insertions(+)
 create mode 100644 libselinux/man/ru/man8/avcstat.8
 create mode 100644 libselinux/man/ru/man8/booleans.8
 create mode 100644 libselinux/man/ru/man8/getenforce.8
 create mode 100644 libselinux/man/ru/man8/getsebool.8
 create mode 100644 libselinux/man/ru/man8/matchpathcon.8
 create mode 100644 libselinux/man/ru/man8/sefcontext_compile.8
 create mode 100644 libselinux/man/ru/man8/selinux.8
 create mode 100644 libselinux/man/ru/man8/selinuxenabled.8
 create mode 100644 libselinux/man/ru/man8/selinuxexeccon.8
 create mode 100644 libselinux/man/ru/man8/setenforce.8
 create mode 100644 libselinux/man/ru/man8/togglesebool.8

diff --git a/libselinux/man/ru/man8/avcstat.8 b/libselinux/man/ru/man8/avcstat.8
new file mode 100644
index 000000000000..b6d84964e7eb
--- /dev/null
+++ b/libselinux/man/ru/man8/avcstat.8
@@ -0,0 +1,35 @@
+.TH "avcstat" "8" "18 ноября 2004" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+avcstat \- показать статистику AVC (Access Vector Cache, кэш вектора доступа) SELinux
+.
+.SH "ОБЗОР"
+.B avcstat
+.RB [ \-c ]
+.RB [ \-f
+.IR status_file ]
+.RI [ interval ]
+.
+.SH "ОПИСАНИЕ"
+Показать статистику AVC SELinux.  Если указан параметр
+.I interval
+, программа будет выполняться циклами, показывая обновлённую статистику каждые
+.I interval
+секунд.
+По умолчанию показываются относительные значения. 
+.
+.SH ПАРАМЕТРЫ
+.TP
+.B \-c
+Показать совокупные значения.
+.TP
+.B \-f
+Указывает расположение файла статистики AVC, по умолчанию это
+.IR /sys/fs/selinux/avc/cache_stats .
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux (8)
+.
+.SH АВТОРЫ
+Эта страница руководства была написана Dan Walsh <dwalsh@redhat.com>.
+Программа была написана James Morris <jmorris@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/booleans.8 b/libselinux/man/ru/man8/booleans.8
new file mode 100644
index 000000000000..20e5b00da25f
--- /dev/null
+++ b/libselinux/man/ru/man8/booleans.8
@@ -0,0 +1,46 @@
+.TH "booleans" "8" "11 августа 2004" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+booleans \- логические переключатели политики позволяют настраивать политику SELinux в среде выполнения
+.
+.SH "ОПИСАНИЕ"
+Эта страница руководства описывает логические переключатели политики SELinux.
+.BR
+Политика SELinux может включать условные правила, которое включены или отключены в зависимости от текущих значений набора логических переключателей политики.
+Эти логические переключатели политики позволяют изменять политику безопасности в среде выполнения без загрузки новой политики.  
+
+Например, логический переключатель httpd_enable_cgi (если он включён) позволяет управляющей программе httpd запускать сценарии cgi. Если администратору требуется запретить исполнение сценариев cgi, можно просто установить соответствующее значение этого переключателя.  
+
+Политика определяет значение по умолчанию для каждого логического переключателя, обычно это false.
+Эти значения по умолчанию можно переопределить через локальные параметры, созданные с помощью утилиты
+.BR setsebool (8)
+, используя
+.B \-P
+для сохранения параметра после перезагрузок.  Средство
+.B system\-config\-securitylevel
+предоставляет графический интерфейс для изменения параметров. Программа
+.BR load_policy (8)
+по умолчанию сохранит текущие параметры логических переключателей после перезагрузки политики по умолчанию. При необходимости также можно сбросить значения логических переключателей на их значения по умолчанию при загрузке, для этого используется параметр 
+.B \-b.
+
+Для получения и вывода списка логических значений служит утилита
+.BR getsebool (8)
+с параметром
+.B \-a.
+
+Логические значения также можно изменить во время выполнения с помощью утилиты
+.BR setsebool (8)
+или утилиты
+.BR togglesebool (8).
+По умолчанию эти утилиты изменяют только текущее логическое значение и не влияют на постоянные параметры, если в setsebool не используется параметр
+.B \-P.
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR getsebool (8),
+.BR setsebool (8),
+.BR selinux (8),
+.BR togglesebool (8)
+.
+.SH АВТОРЫ
+Эта страница руководства была написана Dan Walsh <dwalsh@redhat.com>.
+Поддержка условной политики SELinux была разработана Tresys Technology.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/getenforce.8 b/libselinux/man/ru/man8/getenforce.8
new file mode 100644
index 000000000000..13589e19ae37
--- /dev/null
+++ b/libselinux/man/ru/man8/getenforce.8
@@ -0,0 +1,19 @@
+.TH "getenforce" "8" "7 апреля 2004" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+getenforce \- получить текущий режим SELinux
+.
+.SH "ОБЗОР"
+.B getenforce
+.
+.SH "ОПИСАНИЕ"
+.B getenforce
+сообщает, в каком режиме работает SELinux (принудительный, разрешительный, отключённый).
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux (8),
+.BR setenforce (8),
+.BR selinuxenabled (8)
+.
+.SH АВТОРЫ
+Dan Walsh, <dwalsh@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/getsebool.8 b/libselinux/man/ru/man8/getsebool.8
new file mode 100644
index 000000000000..04d9820ee49d
--- /dev/null
+++ b/libselinux/man/ru/man8/getsebool.8
@@ -0,0 +1,40 @@
+.TH "getsebool" "8" "11 августа 2004" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+getsebool \- получить логические значения SELinux
+.
+.SH "ОБЗОР"
+.B getsebool
+.RB [ \-a ]
+.RI [ boolean ]
+.
+.SH "ОПИСАНИЕ"
+.B getsebool 
+сообщает, где включён или отключён конкретный логический переключатель
+или все логические переключатели SELinux.
+В некоторых ситуациях для логического переключателя может существовать
+ожидающее изменение (переход из одного состояние в другое) - getsebool
+сообщит об этом.
+Ожидающее значение - то значение, которое будет применено при
+следующей фиксации логического переключателя.
+
+Установка значений логических переключателей выполняется в два этапа;
+сначала изменяется ожидающее значение, а затем логические переключатели
+фиксируются, в результате чего их активные значения заменяются 
+ожидающими значениями. Это позволяет изменить группу логических
+переключателей за одну транзацию, задав все необходимые ожидающие
+значения и затем одновременно зафиксировав их.
+.
+.SH ПАРАМЕТРЫ
+.TP
+.B \-a
+Показать все логические переключатели SELinux.
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux (8),
+.BR setsebool (8),
+.BR booleans (8)
+.
+.SH АВТОРЫ
+Эта страница руководства была написана Dan Walsh <dwalsh@redhat.com>.
+Программа была написана Tresys Technology.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/matchpathcon.8 b/libselinux/man/ru/man8/matchpathcon.8
new file mode 100644
index 000000000000..5bd586d47db9
--- /dev/null
+++ b/libselinux/man/ru/man8/matchpathcon.8
@@ -0,0 +1,62 @@
+.TH "matchpathcon" "8" "21 апреля 2005" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+matchpathcon \- получить текущий контекст безопасности SELinux для указанного пути из конфигурации контекстов файлов
+.
+.SH "ОБЗОР"
+.B matchpathcon
+.RB [ \-V ]
+.RB [ \-N ]
+.RB [ \-n ]
+.RB [ \-m
+.IR type ]
+.RB [ \-f
+.IR file_contexts_file ]
+.RB [ \-p
+.IR prefix ]
+.RB [ \-P
+.IR policy_root_path ]
+.I filepath...
+.
+.SH "ОПИСАНИЕ"
+.BR matchpathcon
+опрашивает системную политику и выводит контекст безопасности по умолчанию, связанный с путём к файлу.
+
+.B Примечание:
+Одинаковые пути могут иметь разные контексты безопасности в зависимости от типа файла (обычный файл, каталог, файл связи, файл знаков ...).
+
+.B matchpathcon 
+также будет учитывать тип файла при определении контекста безопасности по умолчанию (если файл существует). Если файл не существует, сопоставление по типу файла не будет выполнено.
+.
+.SH ПАРАМЕТРЫ
+.TP
+.BI \-m " type"
+Принудительно указать тип файла для поиска.
+Действительные типы:
+.BR file ", " dir ", "pipe ", " chr_file ", " blk_file ", "
+.BR lnk_file ", " sock_file .
+.TP
+.B \-n
+Не показывать путь.
+.TP
+.B \-N
+Не использовать преобразования.
+.TP
+.BI \-f " file_context_file"
+Использовать альтернативный файл file_context
+.TP
+.BI \-p " prefix"
+Использовать префикс для ускорения преобразований
+.TP
+.BI \-P " policy_root_path"
+Использовать альтернативный корневой путь к политике
+.TP
+.B \-V
+Проверить контекст файла на диске на соответствие параметрам по умолчанию
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux "(8), "
+.BR matchpathcon (3)
+.
+.SH АВТОРЫ
+Эта страница руководства была написана Dan Walsh <dwalsh@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/sefcontext_compile.8 b/libselinux/man/ru/man8/sefcontext_compile.8
new file mode 100644
index 000000000000..3a6b832a5319
--- /dev/null
+++ b/libselinux/man/ru/man8/sefcontext_compile.8
@@ -0,0 +1,70 @@
+.TH "sefcontext_compile" "8" "12 августа 2015" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+sefcontext_compile \- скомпилировать файлы регулярных выражений контекстов файлов
+.
+.SH "ОБЗОР"
+.B sefcontext_compile
+.RB [ \-o
+.IR outputfile ]
+.RB [ \-p
+.IR policyfile ]
+.I inputfile
+.
+.SH "ОПИСАНИЕ"
+.B sefcontext_compile
+используется для компиляции регулярных выражений контекстов файлов в формат
+.BR pcre (3).
+.sp
+Скомпилированный файл используется функциями проставления меток файлов libselinux.
+.sp
+По умолчанию
+.B sefcontext_compile
+записывает скомпилированный файл pcre с суффиксом
+.B .bin
+в конце (например, \fIinputfile\fB.bin\fR).
+.SH ПАРАМЕТРЫ
+.TP
+.B \-o
+Указать
+.I outputfile
+- должно быть полным именем файла, так как суффикс
+.B .bin
+не добавляется автоматически.
+.TP
+.B \-p
+Указать двоичный
+.I policyfile
+для использования при проверке записей контекста в
+.I inputfile
+.br
+Если найден недействительный контекст, запись файла в формате pcre не будет выполнена и появится сообщение об ошибке.
+
+.SH "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"
+При ошибке возвращается -1. При успешном завершении возвращается 0.
+
+.SH "ПРИМЕРЫ"
+.B Пример 1:
+.br
+sefcontext_compile /etc/selinux/targeted/contexts/files/file_contexts
+.sp
+В результате создаётся следующий файл:
+.RS
+/etc/selinux/targeted/contexts/files/file_contexts.bin
+.RE
+.sp
+.B Пример 2:
+.br
+sefcontext_compile -o new_fc.bin /etc/selinux/targeted/contexts/files/file_contexts
+.sp
+В результате в текущем рабочем каталоге создаётся следующий файл:
+.RS
+new_fc.bin
+.RE
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux (8),
+.BR semanage (8)
+.
+.SH АВТОРЫ
+Dan Walsh, <dwalsh@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/selinux.8 b/libselinux/man/ru/man8/selinux.8
new file mode 100644
index 000000000000..4ab642768be4
--- /dev/null
+++ b/libselinux/man/ru/man8/selinux.8
@@ -0,0 +1,106 @@
+.TH  "selinux"  "8"  "29 апреля 2005" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+SELinux \- Linux с улучшенной безопасностью от (SELinux)
+.
+.SH "ОПИСАНИЕ"
+Linux с улучшенной безопасностью от - это реализация гибкой архитектуры мандатного
+управления доступом в операционной системе Linux. Архитектура SELinux предоставляет
+общую поддержку использования различных видов политик мандатного управления доступом,
+включая основанные на концепциях Type Enforcement® (принудительное присвоение типов),
+Role-Based Access Control (управление доступом на основе ролей) и Multi-Level Security
+(многоуровневая безопасность). Дополнительная информация и техническая документация по
+SELinux доступна по адресу https://github.com/SELinuxProject.
+
+Файл конфигурации
+.I /etc/selinux/config
+позволяет управлять включением и отключением SELinux и, если SELinux включён,
+устанавливать режим его работы - разрешительный или принудительный. Переменной
+.B SELINUX
+можно задать значение отключённой, разрешительной или принудительной, чтобы выбрать
+один из этих вариантов. Если выбрать отключение режима, код ядра и приложения SELinux
+будет полностью отключён, система будет работать без какой-либо защиты SELinux.
+При установке разрешительного режима код SELinux включён, но не выполняет отказы в
+доступе, а только журналирует те действия, которые были бы запрещены при
+принудительном режиме. При установке принудительного режима код SELinux включён,
+выполняет отказы в доступе и журналирует соответствующие попытки доступа. Набор
+отказов в доступе в разрешительном режиме может отличаться от этого набора в
+принудительном режиме как по причине того, что принудительный режим предотвращает
+дальнейшее выполнение операции после первого отказа, так и из-за того, что после
+получения отказа в доступе часть кода приложения вернётся к работе в менее
+привилегированном режиме.
+
+Файл конфигурации
+.I /etc/selinux/config
+также управляет тем, какая политика активна в системе. SELinux позволяет установить
+в системе несколько политик, но одновременно можно использовать только одну из них.
+В настоящее время имеется несколько видов политики SELinux, например, целевая политика
+(targeted), политика многоуровневой безопасности (mls). Целевая политика позволяет
+большинству процессов пользователя выполняться без ограничений, помещая в отдельные
+домены безопасности, ограниченные политикой, только отдельные службы. Например, процессы
+пользователя выполняются в никак не ограниченном домене, в то время как именованная
+управляющая программа или управляющая программа apache будет выполняться в отдельном
+специально настроенном домене. Если используется политика MLS (Multi-Level Security),
+все процессы будут разделены по детально настроенным доменам безопасности и ограничены
+политикой. MLS также поддерживает модель Белла — Лападулы, в которой процессы
+ограничиваются не только по типу, но и по уровню данных.
+
+Чтобы определить, какая политика будет выполняться, следует установить переменную среды
+.B SELINUXTYPE
+в
+.IR /etc/selinux/config .
+Чтобы применить к системе изменение типа политики, необходимо перезагрузить систему и,
+возможно, повторно проставить метки. В каталогах
+.I /etc/selinux/{SELINUXTYPE}/
+необходимо установить для каждой такой политики соответствующую конфигурацию.
+
+Дальнейшую настройку отдельной политики SELinux можно выполнить с помощью набора настраиваемых 
+при компиляции параметров и набора логических переключателей среды выполнения политики.
+.B \%system\-config\-selinux
+позволяет настроить эти логические переключатели и настраиваемые параметры.
+
+Многие домены, которые защищены SELinux, также содержат man-страницы SELinux с информацией
+о настройке соответствующей политики.  
+.
+.SH "ПРОСТАВЛЕНИЕ МЕТОК ДЛЯ ФАЙЛОВ"
+Всем файлам, каталогам, устройствам ... назначены контексты безопасности/метки. Эти контексты хранятся в расширенных атрибутах файловой системы.
+Проблемы с SELinux часто возникают из-за неправильного проставления меток в файловой системе. Это может быть вызвано загрузкой компьютера с ядром, отличным от SELinux. Появление сообщения об ошибке, содержащего file_t, обычно означает серьёзную проблему с проставлением меток в файловой системе.  
+
+Лучшим способом повторного проставления меток в файловой системе является создание файла флага
+.I /.autorelabel
+и последующая перезагрузка.
+.BR system\-config\-selinux
+также имеет эту функциональность. Кроме того, для повторного проставления меток для файлов можно использовать команды
+.BR restorecon / fixfiles.
+.
+.SH ФАЙЛЫ
+.I /etc/selinux/config
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.ad l
+.nh
+.BR booleans (8),
+.BR setsebool (8),
+.BR sepolicy (8),
+.BR system-config-selinux (8),
+.BR togglesebool (8),
+.BR restorecon (8),
+.BR fixfiles (8),
+.BR setfiles (8),
+.BR semanage (8),
+.BR sepolicy (8)
+
+Для каждой ограниченной службы в системе имеется man-cтраница следующего формата:
+.br
+
+.BR <servicename>_selinux (8)
+
+Например, для службы httpd имеется страница
+.BR httpd_selinux (8).
+
+.B man -k selinux
+
+Выведет список всех man-страниц SELinux.
+
+.SH АВТОРЫ
+Эта страница руководства была написана Dan Walsh <dwalsh@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/selinuxenabled.8 b/libselinux/man/ru/man8/selinuxenabled.8
new file mode 100644
index 000000000000..9c4af18b0151
--- /dev/null
+++ b/libselinux/man/ru/man8/selinuxenabled.8
@@ -0,0 +1,21 @@
+.TH "selinuxenabled" "8" "7 апреля 2004" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+selinuxenabled \- утилита для использования внутри сценариев оболочки, которая позволяет определить, включён ли selinux
+.
+.SH "ОБЗОР"
+.B selinuxenabled
+.
+.SH "ОПИСАНИЕ"
+Показывает, включён или отключён SELinux.
+.
+.SH "СОСТОЯНИЕ ВЫХОДА"
+Выход с состоянием 0, если SELinux включён, и 1, если он отключён.
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux (8),
+.BR setenforce (8),
+.BR getenforce (8)
+.
+.SH АВТОРЫ
+Dan Walsh, <dwalsh@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/selinuxexeccon.8 b/libselinux/man/ru/man8/selinuxexeccon.8
new file mode 100644
index 000000000000..3ddfe97b2b0d
--- /dev/null
+++ b/libselinux/man/ru/man8/selinuxexeccon.8
@@ -0,0 +1,28 @@
+.TH "selinuxexeccon" "8" "14 мая 2011" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+selinuxexeccon \- сообщить контекст SELinux, который используется для этого исполняемого файла
+.
+.SH "ОБЗОР"
+.B selinuxexeccon
+.I command
+.RI [ fromcon ]
+.
+.SH "ОПИСАНИЕ"
+.B selinuxexeccon
+сообщает контекст SELinux для указанной команды из указанного контекста или текущего контекста.
+.
+.SH ПРИМЕР
+.nf
+# selinuxexeccon /usr/bin/passwd 
+staff_u:staff_r:passwd_t:s0-s0:c0.c1023
+
+# selinuxexeccon /usr/sbin/sendmail system_u:system_r:httpd_t:s0
+system_u:system_r:system_mail_t:s0
+.fi
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR secon (8)
+.
+.SH АВТОРЫ
+Эта страница руководства была написана Dan Walsh <dwalsh@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/setenforce.8 b/libselinux/man/ru/man8/setenforce.8
new file mode 100644
index 000000000000..e0daad9a25f2
--- /dev/null
+++ b/libselinux/man/ru/man8/setenforce.8
@@ -0,0 +1,32 @@
+.TH "setenforce" "8" "7 апреля 2004" "dwalsh@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+setenforce \- изменить режим, в котором выполняется SELinux
+.
+.SH "ОБЗОР"
+.B setenforce
+.RB [ Enforcing | Permissive | 1 | 0 ]
+.
+.SH "ОПИСАНИЕ"
+Используйте
+.B Enforcing
+или
+.B 1
+для установки SELinux в принудительный режим.
+.br
+Используйте
+.B Permissive
+или
+.B 0
+для установки SELinux в разрешительный режим.
+
+Если SELinux отключён и требуется его включить (или если SELinux включён и требуется его отключить), обратитесь к странице руководства 
+.BR selinux (8).
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux (8),
+.BR getenforce (8),
+.BR selinuxenabled (8)
+.
+.SH АВТОРЫ
+Dan Walsh, <dwalsh@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
diff --git a/libselinux/man/ru/man8/togglesebool.8 b/libselinux/man/ru/man8/togglesebool.8
new file mode 100644
index 000000000000..1da9bcc47147
--- /dev/null
+++ b/libselinux/man/ru/man8/togglesebool.8
@@ -0,0 +1,23 @@
+.TH "togglesebool" "8" "26 октября 2004" "sgrubb@redhat.com" "Документация по командной строке SELinux"
+.SH "ИМЯ"
+togglesebool \- переключить текущее значение логического переключателя SELinux
+.
+.SH "ОБЗОР"
+.B togglesebool
+.I boolean...
+.
+.SH "ОПИСАНИЕ"
+.B togglesebool
+переключает текущее значение списка логических переключателей. Если текущее значение 1,
+то оно будет заменено на 0, и наоборот. Меняются только "находящиеся в памяти" значения;
+параметры загрузки остаются без изменений. 
+.
+.SH "СМОТРИТЕ ТАКЖЕ"
+.BR selinux (8),
+.BR booleans (8),
+.BR getsebool (8),
+.BR setsebool (8)
+.
+.SH АВТОРЫ
+Эта страница руководства была написана Steve Grubb <sgrubb@redhat.com>.
+Перевод на русский язык выполнила Герасименко Олеся <gammaray@basealt.ru>.
-- 
2.41.0

